﻿public class Command {

    public System.Action<string[]> action;
    public Piece[] pieces;

    public Command(System.Action<string[]> action, Piece[] pieces) {
        this.pieces = pieces;
    }

    public void Initialize() {
        foreach (Piece piece in pieces) {
            piece.Initialize();
        }
    }

    public void Submit(string[] args) {
        action.InvokeSafe(args);
    }
    
    public bool IsViable(string[] args, bool newEntry) {
        if (args.Length + (newEntry ? 1 : 0) > pieces.Length)
            return false;

        if (args.Length == 0)
            return true;

        // Check if the last arg is a valid match
        int index = args.Length - 1;
        if (!pieces[index].IsExactOrPartialMatch(args[index]))
            return false;

        // Check if the args before are an exact match
        for (int x = index - 1; x > 0; x--) {
            if (!pieces[x].IsExactMatch(args[x])) {
                return false;
            }
        }

        return true;
    }

    public bool IsMatch(string[] args) {
        if (args.Length != pieces.Length) return false;

        // Loop through all pieces, and return false when one of them isn't an exact match
        for(int i = 0; i < args.Length; i++) {
            if (!pieces[i].IsExactMatch(args[i])) {
                return false;
            }
        }

        return true;
    }

    public override string ToString() {
        return pieces.ToString(" ");
    }
}