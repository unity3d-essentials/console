﻿using System.Linq;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(ConsoleWindow))]
public class ConsoleWindowDebugger : MonoBehaviour {

    public RectTransform commandParent;
    public RectTransform commandText;
    public Text submitOutput;

    private Console console;
    private ConsoleWindow window;

    private Text[] children;
    private int maxPiecesLength { get { return console.commands.OrderByDescending(x => x.pieces.Length).ToList()[0].pieces.Length; } }

    private void Start() {
        window = GetComponent<ConsoleWindow>();
        console = window.console;
        console.onSubmit += Submit;
        console.onAutocomplete += SetViableInputs;

        children = new Text[maxPiecesLength];
        for(int i = 0; i < children.Length; i++) {
            children[i] = Instantiate(commandText).GetComponent<Text>();
            children[i].rectTransform.SetParent(commandParent);
        }
    }

    public void Submit(Command command) {
        submitOutput.text = command == null ? "No viable command was found!" : string.Format("Submitting command <color=orange>{0}</color>", command.ToString());

        foreach (Text t in children)
            t.text = string.Empty;
    }
    
    public void SetViableInputs(string[] inputs, int xIndex, int yIndex) {
        inputs[yIndex] = string.Format("<color=orange>{0}</color>", inputs[yIndex]);
        children[xIndex].GetComponent<Text>().text = inputs.ToString("\n");
    }
}