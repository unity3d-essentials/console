﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ConsoleWindow : MonoBehaviour {

    public Console console;
    public InputField inputField;

    [HideInInspector]
    public List<Command> commands = new List<Command>(){
        new Command(null, new Piece[]{ new Piece("Help"), }),
        new Command(null, new Piece[]{ new Piece("Game"), new Piece(new string[] { "Start", "Pause", "Stop" }), }),
        new Command(null, new Piece[]{ new Piece("Give"), new Piece_GetAvailablePlayers(), new Piece_Int(), new Piece(new string[] { "Gold", "Silver" }), }),
    };

    private void Awake() {
        console = new Console(commands);
    }
    
    public virtual void Submit(string argument) {
        console.Submit(argument);
        ToggleInputField();
    }

    private void Update() {
        if (Input.GetKeyDown(KeyCode.Slash) && !inputField.isFocused) {
            console.Initialize();
            ToggleInputField();
        }

        if (!inputField.interactable) return;

        if (Input.GetKeyDown(KeyCode.Return)) {
            return;
        }

        if (Input.anyKeyDown && !Input.GetKeyDown(KeyCode.Tab)) {
            console.ResetVariables();
            return;
        }

        if (Input.GetKeyDown(KeyCode.Tab)) {
            inputField.text = console.Autocomplete(inputField.text);
            inputField.caretPosition = inputField.text.Length;
        }
    }

    private void ToggleInputField() {
        inputField.interactable = !inputField.interactable;

        if (inputField.interactable) {
            inputField.ActivateInputField();
            inputField.Select();

            inputField.text = "/";
            inputField.caretPosition = inputField.text.Length;

        } else {
            inputField.DeactivateInputField();
            inputField.text = string.Empty;
        }
    }
}