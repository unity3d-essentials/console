# Essentials
Welcome to the Essentials Console repository.

## Information
Console is still a work in progress.

## Getting started
- Go over to https://gitlab.com/unity3d-essentials/console.git and fork the project.
- Open git bash.
- CD to the folder you'd like the submodule be in (alternativaly: create a folder `Submodules` in the `Assets` folder).
- `$git submodule add https://gitlab.com/daandenhartog/console.git`

## Changing code
- Open git bash.
- CD to the submodule directory
- Stage your commits by using `$git add`
- Commit your changes
- Push your changes
- CD back to the main project directory

## Known issues
- Entering two commands which start with the same piece cause only the first command to be caught via autocomplete