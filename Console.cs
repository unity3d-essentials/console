﻿using System.Linq;
using System.Collections.Generic;

public class Console {

    public System.Action<string[], int, int> onAutocomplete;
    public System.Action<Command> onSubmit;
    public System.Action onCommandsChanged;

    public List<Command> commands { get; private set; }
    public List<string> viableInputs { get; private set; }

    public int xIndex { get; private set; }
    public int yIndex { get; private set; }

    public Console(List<Command> commands) {
        this.commands = commands;
    }

    public void Initialize() {
        // Loop through all commands and initialize these
        foreach (Command c in commands)
            c.Initialize();
    }

    public void ResetVariables() {
        viableInputs = null;
        xIndex = 0;
        yIndex = 0;
    }

    public void AddCommand(Command command) {
        if (!commands.Contains(command)) {
            commands.Add(command);

            if (onCommandsChanged != null)
                onCommandsChanged.Invoke();
        }
    }

    public void RemoveCommand(Command command) {
        if (commands.Contains(command)) {
            commands.Remove(command);

            if (onCommandsChanged != null)
                onCommandsChanged.Invoke();
        }
    }

    public Command Submit(string arg) {
        // Loop through all commands and see if a command is viable
        // If it is, call submit and return it
        string[] args = StringToArgs(arg);
        foreach (Command c in commands)
            if (c.IsMatch(args)) {
                if (onSubmit != null)
                    onSubmit.Invoke(c);

                c.Submit(args);
                return c;
            }

        if (onSubmit != null)
            onSubmit.Invoke(null);

        return null;
    }

    public string Autocomplete(string argument) {
        if (commands == null || commands.Count == 0 || argument == string.Empty || argument[0] != '/')
            return argument;

        if(argument[argument.Length - 1] == '/' || argument[argument.Length - 1] == ' ') {
            return AutocompleteNew(StringToArgs(argument), argument);
        } else {
            return AutocompleteExisting(StringToArgs(argument), argument);
        }
    }

    private string AutocompleteNew(string[] args, string argument) {
        xIndex = args.Length;
        viableInputs = GetViableInputs(args, true);
        
        if (viableInputs.Count == 0)
            return argument;

        argument += viableInputs[yIndex];

        if (onAutocomplete != null)
           onAutocomplete.Invoke(viableInputs.ToArray(), xIndex, yIndex);

        yIndex = IncreaseAndLoop(yIndex, viableInputs.Count);

        return argument;
    }

    private string AutocompleteExisting(string[] args, string argument) {
        xIndex = args.Length - 1;

        if (viableInputs == null)
            viableInputs = GetViableInputs(args, false);

        if (viableInputs.Count == 0)
            return argument;

        args[xIndex] = viableInputs[yIndex];

        if (onAutocomplete != null)
            onAutocomplete.Invoke(viableInputs.ToArray(), xIndex, yIndex);

        yIndex = IncreaseAndLoop(yIndex, viableInputs.Count);

        return ArgsToString(args);
    }

    private List<string> GetViableInputs(string[] args, bool newEntry) {
        List<string> inputs = new List<string>();
        int index = newEntry ? args.Length : args.Length - 1;
        string argument = newEntry ? string.Empty : args[index].ToLower();
        
        // Loop through all commands and continue if the command is viable
        // If newEntry is true: Add all options
        // If newEntry is false: Only add options which are either a partial or exact match
        foreach(Command c in commands) {
            if(c.IsViable(args, newEntry)) {
                for(int i = 0; i < c.pieces[index].options.Length; i++) {
                    if(!inputs.Contains(c.pieces[index].options[i]) && (newEntry || (!newEntry && c.pieces[index].CheckForSpecificMatch(argument, i, false)))) {
                        inputs.Add(c.pieces[index].options[i]);
                    }
                }
            }
        }

        return inputs.OrderBy(v => v).ToList();
    }
    
    private string[] StringToArgs(string str) {
        return str.Split(new char[] { ' ', '/' }, System.StringSplitOptions.RemoveEmptyEntries);
    }

    private string ArgsToString(string[] args) {
        return "/" + args.ToString(" ");
    }

    private int IncreaseAndLoop(int integer, int max) {
        return integer + 1 >= max ? 0 : integer + 1;
    }
}