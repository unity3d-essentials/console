﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Piece_GetAvailablePlayers : Piece {

    private string[] availableNames = new string[] {
        "Anthon",
        "Bernard",
        "Cornelus",
        "Diederik",
    };

    public override void Initialize() {
        options = GetRandomNames();
    }

    public override string ToString() {
        return "[players]";
    }

    private string[] GetRandomNames() {
        List<string> names = new List<string>() { "ZemmiPhobia" };
        for(int i = 0; i < availableNames.Length; i++) {
            if (Random.Range(0f, 1f) >= .3f) names.Add(availableNames[i]);
        }

        return names.ToArray();
    }
}