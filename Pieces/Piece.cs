﻿public class Piece {

    public string[] options;
    
    public Piece() : this("") { }
    public Piece(string option) : this(new string[] { option }) { }
    public Piece(string[] options) {
        this.options = options;
    }

    public virtual void Initialize() {

    }

    public virtual bool IsExactMatch(string argument) {
        // Return true when argument is an exact match to any entry in options
        return CheckForAnyMatch(argument.ToLower(), true);
    }

    public virtual bool IsExactOrPartialMatch(string argument) {
        // Return true when argument is a partial or exact match to any entry in options
        return CheckForAnyMatch(argument.ToLower(), false);
    }

    private bool CheckForAnyMatch(string argument, bool exactMatch) {
        for (int i = 0; i < options.Length; i++) {
            if (CheckForSpecificMatch(argument, i, exactMatch)) {
                return true;
            }
        }

        return false;
    }

    public virtual bool CheckForSpecificMatch(string argument, int index, bool exactMatch) {
        return (exactMatch && options[index].ToLower() == argument) || (!exactMatch && options[index].ToLower().StartsWith(argument));
    }

    public override string ToString() {
        return options.ToString("/");
    }
}

public class Piece_Int : Piece {

    public int value;

    public override void Initialize() {
        options = new string[] { "0" };
    }

    public override bool IsExactMatch(string argument) {
        return int.TryParse(argument, out value);
    }

    public override bool CheckForSpecificMatch(string argument, int index, bool exactMatch) {
        return IsExactMatch(argument);
    }

    public override string ToString() {
        return "[number]";
    }
}